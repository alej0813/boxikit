package store.boxi.boxikit.webservices.response;

import android.content.Context;

import org.json.JSONArray;

public class ResponseArrayListener implements IResponseListener<JSONArray> {

    //Request time out (ms)
    public static final int REQUEST_TIME_OUT = 1000 * 30; // 30s

    private Context mContext;

    public ResponseArrayListener(Context context){
        this.mContext = context;
    }

    @Override
    public void getResult(JSONArray object) {
        throw new RuntimeException(mContext.toString()
                + " must implement getResult method");
    }

    @Override
    public void error(JSONArray object) {
        throw new RuntimeException(mContext.toString()
                + " must implement error method");
    }

    @Override
    public void error() {
        throw new RuntimeException(mContext.toString()
                + " must implement error method");
    }
}
