package store.boxi.boxikit.list;

import android.content.Context;
import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONObject;

import store.boxi.boxikit.utils.config.BoxiKitConfig;
import store.boxi.boxikit.utils.listener.IBoxiKitListener;
import store.boxi.boxikit.webservices.response.ResponseArrayListener;
import store.boxi.boxikit.webservices.response.ResponseObjectListener;
import store.boxi.boxikit.webservices.services.DataService;

public class ListHandler {

    private Context mContext;
    private BoxiKitConfig mBoxiKitConfig;

    private String mAuthorization;
    private String mUserAgent;
    private String mClientId;

    public ListHandler(Context context, String authorization, String userAgent, String clientId) {
        mContext = context;
        mBoxiKitConfig = new BoxiKitConfig(context);

        mAuthorization = authorization;
        mUserAgent = userAgent;
        mClientId = clientId;
    }

    public void getUserLists(final IBoxiKitListener listener) {
        List.GetList getList = new List.GetList();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(getList.getPath())
                .build()
                .toString();

        dataService.serverRequest(url, getList.getMethod(), mAuthorization, mUserAgent, mClientId, null, new ResponseArrayListener(mContext) {
            @Override
            public void getResult(JSONArray object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONArray object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }

    public void createUserList(JSONObject data, final IBoxiKitListener listener) {
        List.CreateList createList = new List.CreateList();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(createList.getPath())
                .build()
                .toString();

        dataService.serverRequest(url, createList.getMethod(), mAuthorization, mUserAgent, mClientId, data, new ResponseObjectListener(mContext) {
            @Override
            public void getResult(JSONObject object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONObject object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }

    public void deleteUserList(int listId, final IBoxiKitListener listener) {
        List.DeleteList deleteList = new List.DeleteList();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(String.format(deleteList.getPath(), listId))
                .build()
                .toString();

        dataService.serverRequest(url, deleteList.getMethod(), mAuthorization, mUserAgent, mClientId, null, new ResponseObjectListener(mContext) {
            @Override
            public void getResult(JSONObject object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONObject object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }

    public void addProductToUserList(int listId, JSONObject data, final IBoxiKitListener listener) {
        List.AddProductToList addProductToList = new List.AddProductToList();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(String.format(addProductToList.getPath(), listId))
                .build()
                .toString();

        dataService.serverRequest(url, addProductToList.getMethod(), mAuthorization, mUserAgent, mClientId, data, new ResponseObjectListener(mContext) {
            @Override
            public void getResult(JSONObject object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONObject object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }

    public void addOrderProductToUserList(int listId, JSONArray data, final IBoxiKitListener listener) {
        List.AddOrderProductToList addOrderProductToList = new List.AddOrderProductToList();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(String.format(addOrderProductToList.getPath(), listId))
                .build()
                .toString();

        dataService.serverRequestA(url, addOrderProductToList.getMethod(), mAuthorization, mUserAgent, mClientId, data, new ResponseObjectListener(mContext) {
            @Override
            public void getResult(JSONObject object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONObject object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }

    public void removeProductFromUserList(int listId, int productId, final IBoxiKitListener listener) {
        List.RemoveProductInList removeProductInList = new List.RemoveProductInList();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(String.format(removeProductInList.getPath(), listId, productId))
                .build()
                .toString();

        dataService.serverRequest(url, removeProductInList.getMethod(), mAuthorization, mUserAgent, mClientId, null, new ResponseObjectListener(mContext) {
            @Override
            public void getResult(JSONObject object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONObject object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }

    public void updateProductInUserList(int listId, int productId, JSONObject data, final IBoxiKitListener listener) {
        List.UpdateProductInList updateProductInList = new List.UpdateProductInList();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(String.format(updateProductInList.getPath(), listId, productId))
                .build()
                .toString();

        dataService.serverRequest(url, updateProductInList.getMethod(), mAuthorization, mUserAgent, mClientId, data, new ResponseObjectListener(mContext) {
            @Override
            public void getResult(JSONObject object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONObject object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }

    public void getSharedListInformation(int listId, final IBoxiKitListener listener) {
        List.GetSharedList getSharedList = new List.GetSharedList();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(String.format(getSharedList.getPath(), listId))
                .build()
                .toString();

        dataService.serverRequest(url, getSharedList.getMethod(), mAuthorization, mUserAgent, mClientId, null, new ResponseObjectListener(mContext) {
            @Override
            public void getResult(JSONObject object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONObject object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }

    public void addSharedList(JSONObject data, final IBoxiKitListener listener) {
        List.AddSharedList addSharedList = new List.AddSharedList();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(addSharedList.getPath())
                .build()
                .toString();

        dataService.serverRequest(url, addSharedList.getMethod(), mAuthorization, mUserAgent, mClientId, data, new ResponseObjectListener(mContext) {
            @Override
            public void getResult(JSONObject object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONObject object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }
}
