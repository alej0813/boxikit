package store.boxi.boxikit.cart;

import android.content.Context;
import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONObject;

import store.boxi.boxikit.utils.config.BoxiKitConfig;
import store.boxi.boxikit.utils.listener.IBoxiKitListener;
import store.boxi.boxikit.webservices.response.ResponseArrayListener;
import store.boxi.boxikit.webservices.response.ResponseObjectListener;
import store.boxi.boxikit.webservices.services.DataService;

public class CartHandler {

    private Context mContext;
    private BoxiKitConfig mBoxiKitConfig;

    private String mAuthorization;
    private String mUserAgent;
    private String mClientId;

    public CartHandler(Context context, String authorization, String userAgent, String clientId) {
        mContext = context;
        mBoxiKitConfig = new BoxiKitConfig(context);

        mAuthorization = authorization;
        mUserAgent = userAgent;
        mClientId = clientId;
    }

    public void getUserBox(final IBoxiKitListener listener) {
        Cart.GetBox getBox = new Cart.GetBox();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(getBox.getPath())
                .build()
                .toString();

        dataService.serverRequest(url, getBox.getMethod(), mAuthorization, mUserAgent, mClientId, null, new ResponseArrayListener(mContext) {
            @Override
            public void getResult(JSONArray object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONArray object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }

    public void addProductToUserBox(JSONObject data, final IBoxiKitListener listener) {
        Cart.AddProductToBox addProductToBox = new Cart.AddProductToBox();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(addProductToBox.getPath())
                .build()
                .toString();

        dataService.serverRequest(url, addProductToBox.getMethod(), mAuthorization, mUserAgent, mClientId, data, new ResponseObjectListener(mContext) {
            @Override
            public void getResult(JSONObject object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONObject object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }

    public void updateProductInUserBox(int productId, JSONObject data, final IBoxiKitListener listener) {
        Cart.UpdateProductInBox updateProductInBox = new Cart.UpdateProductInBox();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(String.format(updateProductInBox.getPath(), productId))
                .build()
                .toString();

        dataService.serverRequest(url, updateProductInBox.getMethod(), mAuthorization, mUserAgent, mClientId, data, new ResponseObjectListener(mContext) {
            @Override
            public void getResult(JSONObject object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONObject object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }

    public void removeAllProductInUserBox(final IBoxiKitListener listener) {
        Cart.RemoveAllProductInBox removeAllProductInBox = new Cart.RemoveAllProductInBox();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(removeAllProductInBox.getPath())
                .build()
                .toString();

        dataService.serverRequest(url, removeAllProductInBox.getMethod(), mAuthorization, mUserAgent, mClientId, null, new ResponseObjectListener(mContext) {
            @Override
            public void getResult(JSONObject object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONObject object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }

    public void createUserOrder(JSONObject data, final IBoxiKitListener listener) {
        Cart.CreateOrder createOrder = new Cart.CreateOrder();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(createOrder.getPath())
                .build()
                .toString();

        dataService.serverRequest(url, createOrder.getMethod(), mAuthorization, mUserAgent, mClientId, data, new ResponseObjectListener(mContext) {
            @Override
            public void getResult(JSONObject object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONObject object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }

    public void getDeliveryWindows(final IBoxiKitListener listener) {
        Cart.GetDeliveryWindows getDeliveryWindows = new Cart.GetDeliveryWindows();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(getDeliveryWindows.getPath())
                .build()
                .toString();

        dataService.serverRequest(url, getDeliveryWindows.getMethod(), mAuthorization, mUserAgent, mClientId, null, new ResponseArrayListener(mContext) {
            @Override
            public void getResult(JSONArray object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONArray object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }

    public void getExpressDeliveryWindows(final IBoxiKitListener listener) {
        Cart.GetExpressDeliveryWindows getExpressDeliveryWindows = new Cart.GetExpressDeliveryWindows();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(getExpressDeliveryWindows.getPath())
                .build()
                .toString();

        dataService.serverRequest(url, getExpressDeliveryWindows.getMethod(), mAuthorization, mUserAgent, mClientId, null, new ResponseArrayListener(mContext) {
            @Override
            public void getResult(JSONArray object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONArray object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }
}
