package store.boxi.boxikit.utils.config;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import store.boxi.boxikit.utils.config.Config;

public class BoxiKitConfig {

    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;

    private Gson gson;

    //server connection config
    private static final String KEY_SERVER_CONFIG = "key_server_config";

    // Shared Preference file name
    private static final String PREFERENCE_NAME = "ConfigPreference";

    // Shared preference mode
    private static final int PRIVATE_MODE = 0;

    public BoxiKitConfig(Context context) {
        this.mSharedPreferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        this.mEditor = mSharedPreferences.edit();
        this.mEditor.apply();
        this.gson = new Gson();
    }

    //store server connection scheme
    public void storeServerConfig(Config config) {
        mEditor.putString(KEY_SERVER_CONFIG, gson.toJson(config));
        mEditor.apply();
    }

    //Get server connection scheme
    public Config getServerConfig() {
        String currentSession = mSharedPreferences.getString(KEY_SERVER_CONFIG, null);
        if (gson == null) {
            return null;
        } else {
            try {
                Type typeOfObject = new TypeToken<Config>() {}.getType();
                return gson.fromJson(currentSession, typeOfObject);
            } catch (Exception e) {
                throw new IllegalArgumentException("Object storaged with key " + KEY_SERVER_CONFIG + " is instanceof other class");
            }
        }
    }
}
