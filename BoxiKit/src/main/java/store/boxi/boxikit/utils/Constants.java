package store.boxi.boxikit.utils;

public class Constants {

    public static final String JSON_OBJECT_RESPONSE = "JsonObjectResponse";
    public static final String JSON_ARRAY_RESPONSE = "JsonArrayResponse";
}
