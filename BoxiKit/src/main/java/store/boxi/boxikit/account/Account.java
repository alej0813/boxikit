package store.boxi.boxikit.account;

import com.android.volley.Request;

import store.boxi.boxikit.utils.Constants;

public class Account {

    public static class GetAccount {

        public GetAccount() { }

        public String getPath() { return "users/me"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }

    public static class CreateAccount {

        public CreateAccount() { }

        public String getPath() { return "users"; }

        public int getMethod() { return Request.Method.POST; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }

    public static class UpdateAccount {

        public UpdateAccount() { }

        public String getPath() { return "users/me"; }

        public int getMethod() { return Request.Method.PATCH; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }

    public static class GetAccountImage {

        public GetAccountImage() { }

        public String getPath() { return "sign/images/profile"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }
}
