package store.boxi.boxikit.chat;

import com.android.volley.Request;

import store.boxi.boxikit.utils.Constants;

public class Chat {

    public static class GetChat {

        public GetChat() {}

        public String getPath() { return "chats"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_ARRAY_RESPONSE; }
    }

    public static class GetChatMessage {

        public GetChatMessage() {}

        public String getPath() { return "chats/%1$d/messages"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_ARRAY_RESPONSE; }
    }

    public static class CreateChat {

        public CreateChat() {}

        public String getPath() { return "chats"; }

        public int getMethod() { return Request.Method.POST; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }

    public static class SetReadChatMessage {

        public SetReadChatMessage() {}

        public String getPath() { return "chats/%1$d/messages"; }

        public int getMethod() { return Request.Method.PATCH; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }
}
