package store.boxi.boxikit.webservices.response;

import android.content.Context;

import org.json.JSONObject;

public class ResponseObjectListener implements IResponseListener<JSONObject> {

    //Request time out (ms)
    public static final int REQUEST_TIME_OUT = 1000 * 30; // 30s

    private Context mContext;

    public ResponseObjectListener(Context context){
        this.mContext = context;
    }

    @Override
    public void getResult(JSONObject object) {
        throw new RuntimeException(mContext.toString()
                + " must implement getResult method");
    }

    @Override
    public void error(JSONObject object) {
        throw new RuntimeException(mContext.toString()
                + " must implement error method");
    }

    @Override
    public void error() {
        throw new RuntimeException(mContext.toString()
                + " must implement error method");
    }
}
