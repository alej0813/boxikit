package store.boxi.boxikit.webservices.response;

import org.json.JSONObject;

public interface IResponseListener<T> {
    public void getResult(T object);
    public void error(T object);
    public void error();
}
