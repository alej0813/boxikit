package store.boxi.boxilibrary;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;

/**
 * Created by Alejandro on 30/11/2017.
 */

public class UserAgent {

    private Context mContext;

    public UserAgent(Context context){
        mContext = context;
    }

    public String getUserAgent(){

        String userAgent = null;

        final PackageManager pm = mContext.getPackageManager();
        ApplicationInfo ai;
        PackageInfo packageInfo = null;
        try {
            ai = pm.getApplicationInfo( "store.boxi.boxilibrary", 0);
            packageInfo = pm.getPackageInfo("store.boxi.boxilibrary", 0);
        } catch (final PackageManager.NameNotFoundException e) {
            ai = null;
        }

        String applicationName = (String) (ai != null ? pm.getApplicationLabel(ai) : "(unknown)");
        String versionName = packageInfo.versionName;
        String androidVersion = Build.VERSION.RELEASE;
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;

        userAgent = applicationName + "/" + versionName + " " + manufacturer + "/" + model + " " + "Android/" + androidVersion;
        return userAgent;
    }

    public String getAppVersion(){
        final PackageManager pm = mContext.getPackageManager();
        ApplicationInfo ai;
        PackageInfo packageInfo = null;
        try {
            ai = pm.getApplicationInfo( "store.boxi.boxilibrary", 0);
            packageInfo = pm.getPackageInfo("store.boxi.boxilibrary", 0);
        } catch (final PackageManager.NameNotFoundException e) {
            ai = null;
        }
        //Log.e("VERSION", "VALUE: " + packageInfo.versionName);
        if(packageInfo != null) {
            return packageInfo.versionName;
        } else {
            return null;
        }
    }
}
