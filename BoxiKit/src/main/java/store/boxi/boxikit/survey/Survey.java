package store.boxi.boxikit.survey;

import com.android.volley.Request;

import store.boxi.boxikit.utils.Constants;

public class Survey {

    public static class GetSurvey {

        public GetSurvey() { }

        public String getPath() { return "surveys"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_ARRAY_RESPONSE; }
    }

    public static class GetOrderSurvey {

        public GetOrderSurvey() { }

        public String getPath() { return "orders/%1$d/survey"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_ARRAY_RESPONSE; }
    }

    public static class AnswerSurvey {

        public AnswerSurvey() { }

        public String getPath() { return "surveys/%1$d/answers"; }

        public int getMethod() { return Request.Method.POST; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }

    public static class AnswerQuestionSurvey {

        public AnswerQuestionSurvey() { }

        public String getPath() { return "surveys/%1$d/questions/%2$d/answers"; }

        public int getMethod() { return Request.Method.POST; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }
}
