package store.boxi.boxikit.account;

import android.content.Context;
import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONObject;

import store.boxi.boxikit.utils.config.BoxiKitConfig;
import store.boxi.boxikit.utils.listener.IBoxiKitListener;
import store.boxi.boxikit.webservices.response.ResponseArrayListener;
import store.boxi.boxikit.webservices.response.ResponseObjectListener;
import store.boxi.boxikit.webservices.services.DataService;

public class AccountHandler {

    private Context mContext;
    private BoxiKitConfig mBoxiKitConfig;

    private String mAuthorization;
    private String mUserAgent;
    private String mClientId;

    public AccountHandler(Context context, String authorization, String userAgent, String clientId) {
        mContext = context;
        mBoxiKitConfig = new BoxiKitConfig(mContext);

        mAuthorization = authorization;
        mUserAgent = userAgent;
        mClientId = clientId;
    }

    public void getUserAccount(final IBoxiKitListener listener) {
        Account.GetAccount getAccount = new Account.GetAccount();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(getAccount.getPath())
                .build()
                .toString();

        dataService.serverRequest(url, getAccount.getMethod(), mAuthorization, mUserAgent, mClientId, null, new ResponseObjectListener(mContext) {
                @Override
                public void getResult(JSONObject object) {
                    listener.getResult(object);
                }

                @Override
                public void error(JSONObject object) {
                    listener.error(object);
                }

                @Override
                public void error() {
                    listener.error();
                }
            });
    }

    public void createUserAccount(JSONObject data, final IBoxiKitListener listener) {
        Account.CreateAccount createAccount = new Account.CreateAccount();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(createAccount.getPath())
                .build()
                .toString();

        dataService.serverRequest(url, createAccount.getMethod(), mAuthorization, mUserAgent, mClientId, data, new ResponseObjectListener(mContext) {
                @Override
                public void getResult(JSONObject object) {
                    listener.getResult(object);
                }

                @Override
                public void error(JSONObject object) {
                    listener.error(object);
                }

                @Override
                public void error() {
                    listener.error();
                }
            });
    }

    public void updateUserAccount(JSONObject data, final IBoxiKitListener listener) {
        Account.UpdateAccount updateAccount = new Account.UpdateAccount();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(updateAccount.getPath())
                .build()
                .toString();

        dataService.serverRequest(url, updateAccount.getMethod(), mAuthorization, mUserAgent, mClientId, data, new ResponseObjectListener(mContext) {
            @Override
            public void getResult(JSONObject object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONObject object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }

    public void getUserImageUploadUrl(String filename, final IBoxiKitListener listener) {
        Account.GetAccountImage getAccountImage = new Account.GetAccountImage();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(getAccountImage.getPath())
                .appendQueryParameter("file", filename)
                .build()
                .toString();

        dataService.serverRequest(url, getAccountImage.getMethod(), mAuthorization, mUserAgent, mClientId, null, new ResponseObjectListener(mContext) {
            @Override
            public void getResult(JSONObject object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONObject object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }
}
