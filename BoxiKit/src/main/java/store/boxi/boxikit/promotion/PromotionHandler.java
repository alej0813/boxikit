package store.boxi.boxikit.promotion;

import android.content.Context;
import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONObject;

import store.boxi.boxikit.utils.config.BoxiKitConfig;
import store.boxi.boxikit.utils.listener.IBoxiKitListener;
import store.boxi.boxikit.webservices.response.ResponseArrayListener;
import store.boxi.boxikit.webservices.response.ResponseObjectListener;
import store.boxi.boxikit.webservices.services.DataService;

public class PromotionHandler {

    private Context mContext;
    private BoxiKitConfig mBoxiKitConfig;

    private String mAuthorization;
    private String mUserAgent;
    private String mClientId;

    public PromotionHandler(Context context, String authorization, String userAgent, String clientId) {
        mContext = context;
        mBoxiKitConfig = new BoxiKitConfig(context);

        mAuthorization = authorization;
        mUserAgent = userAgent;
        mClientId = clientId;
    }

    public void getUserPromotions(final IBoxiKitListener listener) {
        Promotion.GetUserPromotion getUserPromotion = new Promotion.GetUserPromotion();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(getUserPromotion.getPath())
                .build()
                .toString();

        dataService.serverRequest(url, getUserPromotion.getMethod(), mAuthorization, mUserAgent, mClientId, null, new ResponseArrayListener(mContext) {
            @Override
            public void getResult(JSONArray object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONArray object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }

    public void getPromotionsModals(final IBoxiKitListener listener) {
        Promotion.GetPromotionModal getPromotionModal = new Promotion.GetPromotionModal();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(getPromotionModal.getPath())
                .build()
                .toString();

        dataService.serverRequest(url, getPromotionModal.getMethod(), mAuthorization, mUserAgent, mClientId, null, new ResponseArrayListener(mContext) {
            @Override
            public void getResult(JSONArray object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONArray object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }

    public void setUserPromotionModalAsViewed(int modalId, final IBoxiKitListener listener) {
        Promotion.SetPromotionModal setPromotionModal = new Promotion.SetPromotionModal();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(String.format(setPromotionModal.getPath(), modalId))
                .build()
                .toString();

        dataService.serverRequest(url, setPromotionModal.getMethod(), mAuthorization, mUserAgent, mClientId, null, new ResponseObjectListener(mContext) {
            @Override
            public void getResult(JSONObject object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONObject object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }

    public void setUserActivePromotion(String promotionCode, final IBoxiKitListener listener) {
        Promotion.SetUserPromotion setUserPromotion = new Promotion.SetUserPromotion();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(String.format(setUserPromotion.getPath(), promotionCode))
                .build()
                .toString();

        dataService.serverRequest(url, setUserPromotion.getMethod(), mAuthorization, mUserAgent, mClientId, null, new ResponseObjectListener(mContext) {
            @Override
            public void getResult(JSONObject object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONObject object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }
}
