package store.boxi.boxikit.list;

import com.android.volley.Request;

import store.boxi.boxikit.utils.Constants;

public class List {

    public static class GetList {

        public GetList() {}

        public String getPath() { return "users/me/lists"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_ARRAY_RESPONSE; }
    }

    public static class CreateList {

        public CreateList() {}

        public String getPath() { return "users/me/lists"; }

        public int getMethod() { return Request.Method.POST; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }

    public static class DeleteList {

        public DeleteList() {}

        public String getPath() { return "users/me/lists/%1$d"; }

        public int getMethod() { return Request.Method.DELETE; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }

    public static class AddProductToList {

        public AddProductToList() {}

        public String getPath() { return "users/me/lists/%1$d/products"; }

        public int getMethod() { return Request.Method.PUT; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }

    public static class RemoveProductInList {

        public RemoveProductInList() {}

        public String getPath() { return "users/me/lists/%1$d/products/%2$d"; }

        public int getMethod() { return Request.Method.DELETE; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }

    public static class UpdateProductInList {

        public UpdateProductInList() {}

        public String getPath() { return "users/me/lists/%1$d/products/%2$d"; }

        public int getMethod() { return Request.Method.PATCH; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }

    public static class AddOrderProductToList {

        public AddOrderProductToList() {}

        public String getPath() { return "users/me/lists/%1$d/products"; }

        public int getMethod() { return Request.Method.PUT; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }

    public static class GetSharedList {

        public GetSharedList() {}

        public String getPath() { return "lists/%1$d"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }

    public static class AddSharedList {

        public AddSharedList() {}

        public String getPath() { return "users/me/lists"; }

        public int getMethod() { return Request.Method.PUT; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }
}
