package store.boxi.boxikit.webservices;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

public class CustomJsonObjectRequest extends JsonObjectRequest {

    private JSONObject mRequestObject;
    private Response.Listener<JSONObject> mResponseListener;

    public CustomJsonObjectRequest(int method, String url, JSONObject data, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, url, data, listener, errorListener);
        mRequestObject = data;
        mResponseListener = listener;
    }

    @Override
    protected void deliverResponse(JSONObject response) {
        mResponseListener.onResponse(response);
    }

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            //Log.e("JSON", "VALUE: " + json);

            JSONObject result = null;

            if(json != null && json.length() > 0) {
                result = new JSONObject(json);
            }

            return Response.success(result,
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (Exception e) {
            return Response.error(new ParseError(e));
        }
    }
}
