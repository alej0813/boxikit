package store.boxi.boxikit.cart;

import com.android.volley.Request;

import store.boxi.boxikit.utils.Constants;

public class Cart {

    public static class GetBox {

        public GetBox() {}

        public String getPath() { return "users/me/box"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_ARRAY_RESPONSE; }
    }

    public static class AddProductToBox {

        public AddProductToBox() {}

        public String getPath() { return "users/me/box/products"; }

        public int getMethod() { return Request.Method.PUT; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }

    public static class UpdateProductInBox {

        public UpdateProductInBox() {}

        public String getPath() { return "users/me/box/products/%1$d"; }

        public int getMethod() { return Request.Method.PATCH; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }

    public static class RemoveAllProductInBox {

        public RemoveAllProductInBox() {}

        public String getPath() { return "users/me/box"; }

        public int getMethod() { return Request.Method.DELETE; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }

    public static class CreateOrder {

        public CreateOrder() {}

        public String getPath() { return "orders"; }

        public int getMethod() { return Request.Method.POST; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }

    public static class GetDeliveryWindows {

        public GetDeliveryWindows() {}

        public String getPath() { return "delivery/windows"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_ARRAY_RESPONSE; }
    }

    public static class GetExpressDeliveryWindows {

        public GetExpressDeliveryWindows() {}

        public String getPath() { return "delivery/windows/relative"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_ARRAY_RESPONSE; }
    }
}
