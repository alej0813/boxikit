package store.boxi.boxikit.product;

import com.android.volley.Request;

import store.boxi.boxikit.utils.Constants;

public class Product {

    public static class GetProductInfo {

        public GetProductInfo() {}

        public String getPath() { return "products/%1$d"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }

    public static class GetBestSeller {

        public GetBestSeller() {}

        public String getPath() { return "home/products"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_ARRAY_RESPONSE; }
    }

    public static class GetTourProduct {

        public GetTourProduct() {}

        public String getPath() { return "products"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_ARRAY_RESPONSE; }
    }

    public static class GetNewProduct {

        public GetNewProduct() {}

        public String getPath() { return "home/products/new"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_ARRAY_RESPONSE; }
    }
}
