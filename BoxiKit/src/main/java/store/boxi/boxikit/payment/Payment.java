package store.boxi.boxikit.payment;

import com.android.volley.Request;

import store.boxi.boxikit.utils.Constants;

public class Payment {

    public static class GetPayment {

        public GetPayment() { }

        public String getPath() { return "users/me/cards"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_ARRAY_RESPONSE; }
    }

    public static class CreatePayment {

        public CreatePayment() { }

        public String getPath() { return "users/me/cards"; }

        public int getMethod() { return Request.Method.POST; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }

    public static class DeletePayment {

        public DeletePayment() { }

        public String getPath() { return "users/me/cards/%1$d"; }

        public int getMethod() { return Request.Method.DELETE; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }
}
