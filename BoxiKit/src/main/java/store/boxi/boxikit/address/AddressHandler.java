package store.boxi.boxikit.address;

import android.content.Context;
import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONObject;

import store.boxi.boxikit.utils.config.BoxiKitConfig;
import store.boxi.boxikit.utils.listener.IBoxiKitListener;
import store.boxi.boxikit.webservices.response.ResponseArrayListener;
import store.boxi.boxikit.webservices.response.ResponseObjectListener;
import store.boxi.boxikit.webservices.services.DataService;

public class AddressHandler {

    private Context mContext;
    private BoxiKitConfig mBoxiKitConfig;

    private String mAuthorization;
    private String mUserAgent;
    private String mClientId;

    public AddressHandler(Context context, String authorization, String userAgent, String clientId) {
        mContext = context;
        mBoxiKitConfig = new BoxiKitConfig(context);

        mAuthorization = authorization;
        mUserAgent = userAgent;
        mClientId = clientId;
    }

    public void getUserAddresses(final IBoxiKitListener listener) {
        Address.GetAddress getAddress = new Address.GetAddress();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(getAddress.getPath())
                .build()
                .toString();

        dataService.serverRequest(url, getAddress.getMethod(), mAuthorization, mUserAgent, mClientId, null, new ResponseArrayListener(mContext) {
            @Override
            public void getResult(JSONArray object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONArray object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }

    public void createUserAddress(JSONObject data, final IBoxiKitListener listener) {
        Address.CreateAddress createAddress = new Address.CreateAddress();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(createAddress.getPath())
                .build()
                .toString();

        dataService.serverRequest(url, createAddress.getMethod(), mAuthorization, mUserAgent, mClientId, data, new ResponseObjectListener(mContext) {
            @Override
            public void getResult(JSONObject object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONObject object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }

    public void deleteUserAddress(int addressId, final IBoxiKitListener listener) {
        Address.DeleteAddress deleteAddress = new Address.DeleteAddress();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(String.format(deleteAddress.getPath(), addressId))
                .build()
                .toString();

        dataService.serverRequest(url, deleteAddress.getMethod(), mAuthorization, mUserAgent, mClientId, null, new ResponseObjectListener(mContext) {
            @Override
            public void getResult(JSONObject object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONObject object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }

    public void getDeliveryAreas(final IBoxiKitListener listener) {
        Address.GetDeliveryArea getDeliveryArea = new Address.GetDeliveryArea();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(getDeliveryArea.getPath())
                .build()
                .toString();

        dataService.serverRequest(url, getDeliveryArea.getMethod(), mAuthorization, mUserAgent, mClientId, null, new ResponseArrayListener(mContext) {
            @Override
            public void getResult(JSONArray object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONArray object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }
}
