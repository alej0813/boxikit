package store.boxi.boxikit.banner;

import com.android.volley.Request;

import store.boxi.boxikit.utils.Constants;

public class Banner {

    public static class GetBanner {

        public GetBanner() { }

        public String getPath() { return "banners"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_ARRAY_RESPONSE; }
    }

    public static class GetLargeBanner {

        public GetLargeBanner() { }

        public String getPath() { return "banners/large"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_ARRAY_RESPONSE; }
    }
}
