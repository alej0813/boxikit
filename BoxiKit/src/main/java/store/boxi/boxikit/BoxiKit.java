package store.boxi.boxikit;

import android.content.Context;

import store.boxi.boxikit.utils.config.BoxiKitConfig;
import store.boxi.boxikit.utils.config.Config;

public class BoxiKit {

    private static final String TAG = "BoxiKit";

    public static void setServerConfiguration(Context context, String serverScheme, String serverUrl, String serverApi, String serverVersion) {
        Config serverConfig = new Config(serverScheme, serverUrl, serverApi, serverVersion);

        BoxiKitConfig mBoxiKitConfig = new BoxiKitConfig(context);
        mBoxiKitConfig.storeServerConfig(serverConfig);
    }
}
