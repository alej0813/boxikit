package store.boxi.boxilibrary;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import store.boxi.boxikit.BoxiKit;
import store.boxi.boxikit.account.AccountHandler;
import store.boxi.boxikit.utils.listener.BoxiKitListener;

public class MainActivity extends AppCompatActivity {

    private ProgressBar mProgressBar;
    private Button mDataButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mProgressBar = findViewById(R.id.hello_progress);
        mProgressBar.setVisibility(View.GONE);

        mDataButton = findViewById(R.id.hello_button);
        mDataButton.setVisibility(View.VISIBLE);
        mDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProgressBar.setVisibility(View.VISIBLE);
                mDataButton.setVisibility(View.GONE);

                BoxiKit.setServerConfiguration(MainActivity.this, "https", "marketplace-dev.comunitaasservices.com",
                        "marketplace-dev.comunitaasservices.com/v1", "v1");

                /*AccountHandler accountHandler = new AccountHandler(MainActivity.this, "Bearer aagsargr", new UserAgent(MainActivity.this).getUserAgent(), "");
                accountHandler.getUserAccount(new BoxiKitListener(MainActivity.this) {
                    @Override
                    public void getResult(Object object) {
                        Log.e("RESULT", "VALUE: " + object);

                        mDataButton.setVisibility(View.VISIBLE);
                        mProgressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void error(Object object) {
                        Log.e("ERROR", "VALUE: " + object);

                        mDataButton.setVisibility(View.VISIBLE);
                        mProgressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void error() {
                        Log.e("ERROR", "ERROR");

                        mDataButton.setVisibility(View.VISIBLE);
                        mProgressBar.setVisibility(View.GONE);
                    }
                });*/
            }
        });
    }
}