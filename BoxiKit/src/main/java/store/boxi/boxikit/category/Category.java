package store.boxi.boxikit.category;

import com.android.volley.Request;

import store.boxi.boxikit.utils.Constants;

public class Category {

    public static class GetCategory {

        public GetCategory() {}

        public String getPath() { return "categories"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_ARRAY_RESPONSE; }
    }

    public static class GetCategoryProduct {

        public GetCategoryProduct() {}

        public String getPath() { return "categories/%1$d/products"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_ARRAY_RESPONSE; }
    }

    public static class GetCategoryProductRelated {

        public GetCategoryProductRelated() {}

        public String getPath() { return "categories/%1$d/related"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_ARRAY_RESPONSE; }
    }
}
