package store.boxi.boxikit.deal;

import com.android.volley.Request;

import store.boxi.boxikit.utils.Constants;

public class Deal {

    public static class GetDeal {

        public GetDeal() {}

        public String getPath() { return "discounts"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_ARRAY_RESPONSE; }
    }

    public static class GetDealProduct {

        public GetDealProduct() {}

        public String getPath() { return "deals/%1$d/products"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_ARRAY_RESPONSE; }
    }

    public static class GetDealDeals {

        public GetDealDeals() {}

        public String getPath() { return "deals"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_ARRAY_RESPONSE; }
    }
}
