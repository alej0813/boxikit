package store.boxi.boxikit.webservices.services;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import store.boxi.boxikit.webservices.CustomJsonObjectArrayRequest;
import store.boxi.boxikit.webservices.CustomJsonObjectRequest;
import store.boxi.boxikit.webservices.RequestQueue;
import store.boxi.boxikit.webservices.response.ResponseArrayListener;
import store.boxi.boxikit.webservices.response.IResponseListener;

public class DataService {

    private Context mContext;

    public DataService(Context context) {
        mContext = context;
    }

    /**
     * Function use to get data from the server that response with an array
     * @param listener - the listener to obtain the response from the server
     */
    public void serverRequest(String url, int method, String authorization, String userAgent, String clientId, JSONArray data, final IResponseListener<JSONArray> listener) {

        JsonArrayRequest request = new JsonArrayRequest
                (method, url, data, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        listener.getResult(response);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String body;
                        try {
                            if(error.networkResponse != null){
                                if(error.networkResponse.data.length != 0){
                                    body = new String(error.networkResponse.data, StandardCharsets.UTF_8);
                                    JSONArray errorArray = new JSONArray();
                                    errorArray.put(body);
                                    listener.error(errorArray);
                                }
                            }
                            else {
                                listener.error();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.error();
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", authorization);
                headers.put("User-Agent", userAgent);

                return headers;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(
                ResponseArrayListener.REQUEST_TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue.getInstance(mContext).addToRequestQueue(request);
    }

    public void serverRequest(String url, int method, String authorization, String userAgent, String clientId, JSONObject data, final IResponseListener<JSONObject> listener) {

        CustomJsonObjectRequest request = new CustomJsonObjectRequest
                (method, url, data, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        listener.getResult(response);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String body;
                        try {
                            if(error.networkResponse != null){
                                if(error.networkResponse.data.length != 0){
                                    body = new String(error.networkResponse.data, StandardCharsets.UTF_8);
                                    JSONObject errorObject = new JSONObject(body);
                                    listener.error(errorObject);
                                }
                            }
                            else {
                                listener.error();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.error();
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", authorization);
                headers.put("User-Agent", userAgent);

                return headers;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(
                ResponseArrayListener.REQUEST_TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue.getInstance(mContext).addToRequestQueue(request);
    }

    public void serverRequestA(String url, int method, String authorization, String userAgent, String clientId, JSONArray data, final IResponseListener<JSONObject> listener) {

        CustomJsonObjectArrayRequest request = new CustomJsonObjectArrayRequest
                (method, url, data, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        listener.getResult(response);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String body;
                        try {
                            if(error.networkResponse != null){
                                if(error.networkResponse.data.length != 0){
                                    body = new String(error.networkResponse.data, StandardCharsets.UTF_8);
                                    JSONObject errorObject = new JSONObject(body);
                                    listener.error(errorObject);
                                }
                            }
                            else {
                                listener.error();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.error();
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", authorization);
                headers.put("User-Agent", userAgent);

                return headers;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(
                ResponseArrayListener.REQUEST_TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue.getInstance(mContext).addToRequestQueue(request);
    }
}
