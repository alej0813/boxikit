package store.boxi.boxikit.chat;

import android.content.Context;
import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONObject;

import store.boxi.boxikit.utils.config.BoxiKitConfig;
import store.boxi.boxikit.utils.listener.IBoxiKitListener;
import store.boxi.boxikit.webservices.response.ResponseArrayListener;
import store.boxi.boxikit.webservices.response.ResponseObjectListener;
import store.boxi.boxikit.webservices.services.DataService;

public class ChatHandler {

    private Context mContext;
    private BoxiKitConfig mBoxiKitConfig;

    private String mAuthorization;
    private String mUserAgent;
    private String mClientId;

    public ChatHandler(Context context, String authorization, String userAgent, String clientId) {
        mContext = context;
        mBoxiKitConfig = new BoxiKitConfig(context);

        mAuthorization = authorization;
        mUserAgent = userAgent;
        mClientId = clientId;
    }

    public void getUserChats(final IBoxiKitListener listener) {
        Chat.GetChat getChat = new Chat.GetChat();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(getChat.getPath())
                .build()
                .toString();

        dataService.serverRequest(url, getChat.getMethod(), mAuthorization, mUserAgent, mClientId, null, new ResponseArrayListener(mContext) {
            @Override
            public void getResult(JSONArray object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONArray object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }

    public void getChatMessages(int chatId, final IBoxiKitListener listener) {
        Chat.GetChatMessage getChatMessage = new Chat.GetChatMessage();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(String.format(getChatMessage.getPath(), chatId))
                .build()
                .toString();

        dataService.serverRequest(url, getChatMessage.getMethod(), mAuthorization, mUserAgent, mClientId, null, new ResponseArrayListener(mContext) {
            @Override
            public void getResult(JSONArray object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONArray object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }

    public void createOrderChat(JSONObject data, final IBoxiKitListener listener) {
        Chat.CreateChat createChat = new Chat.CreateChat();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(createChat.getPath())
                .build()
                .toString();

        dataService.serverRequest(url, createChat.getMethod(), mAuthorization, mUserAgent, mClientId, data, new ResponseObjectListener(mContext) {
            @Override
            public void getResult(JSONObject object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONObject object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }

    public void setReadChatMessages(int chatId, JSONArray data, final IBoxiKitListener listener) {
        Chat.SetReadChatMessage setReadChatMessage = new Chat.SetReadChatMessage();
        DataService dataService = new DataService(mContext);

        String url = new Uri.Builder()
                .scheme(mBoxiKitConfig.getServerConfig().getServerScheme())
                .encodedAuthority(mBoxiKitConfig.getServerConfig().getServerApi())
                .appendEncodedPath(String.format(setReadChatMessage.getPath(), chatId))
                .build()
                .toString();

        dataService.serverRequestA(url, setReadChatMessage.getMethod(), mAuthorization, mUserAgent, mClientId, data, new ResponseObjectListener(mContext) {
            @Override
            public void getResult(JSONObject object) {
                listener.getResult(object);
            }

            @Override
            public void error(JSONObject object) {
                listener.error(object);
            }

            @Override
            public void error() {
                listener.error();
            }
        });
    }
}
