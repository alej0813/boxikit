package store.boxi.boxikit.search;

import com.android.volley.Request;

import store.boxi.boxikit.utils.Constants;

public class Search {

    public static class GetSearchResult {

        public GetSearchResult() {}

        public String getPath() { return "search"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_ARRAY_RESPONSE; }
    }

    public static class GetSuggestionResult {

        public GetSuggestionResult() {}

        public String getPath() { return "suggestions"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_ARRAY_RESPONSE; }
    }
}
