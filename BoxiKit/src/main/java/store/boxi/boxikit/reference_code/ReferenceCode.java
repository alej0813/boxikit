package store.boxi.boxikit.reference_code;

import com.android.volley.Request;

import store.boxi.boxikit.utils.Constants;

public class ReferenceCode {

    public static class GetReferenceCodeInfo {

        public GetReferenceCodeInfo() { }

        public String getPath() { return "references/%1$s"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }

    public static class ApplyReferenceCode {

        public ApplyReferenceCode() { }

        public String getPath() { return "references/%1$s"; }

        public int getMethod() { return Request.Method.POST; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }
}
