package store.boxi.boxikit.order;

import com.android.volley.Request;

import store.boxi.boxikit.utils.Constants;

public class Order {

    public static class GetOrder {

        public GetOrder() { }

        public String getPath() { return "orders"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_ARRAY_RESPONSE; }
    }

    public static class GetCompletedOrder {

        public GetCompletedOrder() { }

        public String getPath() { return "orders/history"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_ARRAY_RESPONSE; }
    }

    public static class GetOrderInformation {

        public GetOrderInformation() { }

        public String getPath() { return "orders/%1$d"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }
}
