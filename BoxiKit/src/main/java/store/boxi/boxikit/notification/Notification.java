package store.boxi.boxikit.notification;

import com.android.volley.Request;

import store.boxi.boxikit.utils.Constants;

public class Notification {

    public static class GetNotification {

        public GetNotification() { }

        public String getPath() { return "users/me/notifications"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_ARRAY_RESPONSE; }
    }

    public static class GetNotificationInformation {

        public GetNotificationInformation() { }

        public String getPath() { return "users/me/notifications/%1$d"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }

    public static class SetNotificationRead {

        public SetNotificationRead() { }

        public String getPath() { return "users/me/notifications/%1$d"; }

        public int getMethod() { return Request.Method.PATCH; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }

}
