package store.boxi.boxikit.utils.config;

public class Config {

    private String server_scheme;
    private String server_url;
    private String server_api;
    private String server_version;

    public Config(String scheme, String url, String api, String version) {
        this.server_scheme = scheme;
        this.server_url = url;
        this.server_api = api;
        this.server_version = version;
    }

    public String getServerScheme() { return server_scheme; }

    public String getServerUrl() { return server_url; }

    public String getServerApi() { return server_api; }

    public String getServerVersion() { return server_version; }
}
