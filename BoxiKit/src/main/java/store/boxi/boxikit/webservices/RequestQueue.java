package store.boxi.boxikit.webservices;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.toolbox.Volley;

public class RequestQueue {

    private RequestQueue(Context context) {
        sContext = context;
        mRequestQueue = getRequestQueue();
    }

    public com.android.volley.RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(sContext.getApplicationContext());
        }
        return mRequestQueue;
    }

    public static synchronized RequestQueue getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new RequestQueue(context);
        }
        return sInstance;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

    private static RequestQueue sInstance;
    private com.android.volley.RequestQueue mRequestQueue;
    private static Context sContext;
}
