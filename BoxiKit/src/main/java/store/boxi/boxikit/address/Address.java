package store.boxi.boxikit.address;

import com.android.volley.Request;

import store.boxi.boxikit.utils.Constants;

public class Address {

    public static class GetAddress {

        public GetAddress() {}

        public String getPath() { return "users/me/addresses"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_ARRAY_RESPONSE; }
    }

    public static class CreateAddress {

        public CreateAddress() {}

        public String getPath() { return "users/me/addresses"; }

        public int getMethod() { return Request.Method.POST; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }

    public static class DeleteAddress {

        public DeleteAddress() {}

        public String getPath() { return "users/me/addresses/%1$d"; }

        public int getMethod() { return Request.Method.DELETE; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }

    public static class GetDeliveryArea {

        public GetDeliveryArea() {}

        public String getPath() { return "delivery/areas"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_ARRAY_RESPONSE; }
    }
}
