package store.boxi.boxikit.utils.listener;

public interface IBoxiKitListener {
    public void getResult(Object object);
    public void error(Object object);
    public void error();
}
