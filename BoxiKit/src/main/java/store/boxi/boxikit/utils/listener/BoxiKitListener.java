package store.boxi.boxikit.utils.listener;

import android.content.Context;

public class BoxiKitListener implements IBoxiKitListener {

    //Request time out (ms)
    public static final int REQUEST_TIME_OUT = 1000 * 30; // 30s

    private Context mContext;

    public BoxiKitListener(Context context){
        this.mContext = context;
    }

    @Override
    public void getResult(Object object) {
        throw new RuntimeException(mContext.toString()
                + " must implement getResult method");
    }

    @Override
    public void error(Object object) {
        throw new RuntimeException(mContext.toString()
                + " must implement error method");
    }

    @Override
    public void error() {
        throw new RuntimeException(mContext.toString()
                + " must implement error method");
    }
}
