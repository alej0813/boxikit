package store.boxi.boxikit.promotion;

import com.android.volley.Request;

import store.boxi.boxikit.utils.Constants;

public class Promotion {

    public static class GetUserPromotion {

        public GetUserPromotion() { }

        public String getPath() { return "users/me/promotions"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_ARRAY_RESPONSE; }
    }

    public static class GetPromotionModal {

        public GetPromotionModal() { }

        public String getPath() { return "home/modals/promotions"; }

        public int getMethod() { return Request.Method.GET; }

        public String getResponse() { return Constants.JSON_ARRAY_RESPONSE; }
    }

    public static class SetUserPromotion {

        public SetUserPromotion() { }

        public String getPath() { return "users/me/promotions/%1$s"; }

        public int getMethod() { return Request.Method.PUT; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }

    public static class SetPromotionModal {

        public SetPromotionModal() { }

        public String getPath() { return "home/modals/promotions/%1$d"; }

        public int getMethod() { return Request.Method.PATCH; }

        public String getResponse() { return Constants.JSON_OBJECT_RESPONSE; }
    }
}
